// Obtener alturas reales del navegador considerando el header y el footer

function getSizes() {
    var header = document.getElementById("main-header");
    var footer = document.getElementById("main-footer");

    function resizeAction() {

        if (header) {
            let headerHeight = header.clientHeight;
            document.documentElement.style.setProperty('--header', `${headerHeight}px`);
        } else {
            document.documentElement.style.setProperty('--header', 0);
        }

        if (footer) {
            let footerHeight = footer.clientHeight;
            document.documentElement.style.setProperty('--footer', `${footerHeight}px`);
        } else {
            document.documentElement.style.setProperty('--footer', 0);
        }

        let scrollBarWidth = window.innerWidth - document.documentElement.getBoundingClientRect().width;
        document.documentElement.style.setProperty('--scrollbar', `${scrollBarWidth}px`);

        // Altura verdadera del navegador, 100vh a veces no funciona en navegadores para celular
        document.documentElement.style.setProperty('--app-height', `${window.innerHeight}px`)

    }

    var onResize;
    onResize = setTimeout(resizeAction, 100);

    addEventListener("resize", function () {
        clearTimeout(onResize);
        onResize = setTimeout(resizeAction, 100);
    });
}

document.addEventListener("DOMContentLoaded", getSizes);
